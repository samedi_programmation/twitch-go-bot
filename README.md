############################
# Les Samedi Programmation #
############################


Samedi 13/06/2020 : Premier bot chat Twitch
===========================================


Ce code source permet de facilement créer un bot Twitch (en Golang).


Il permet de gérer des commandes dans le chat (avec et sans paramètres).
Des messages automatiques sur timer sont aussi disponibles.

La notification de départ du chat est fonctionnelle.

Pour compiler et obtenir un exécutable, go build twitch-go-bot.go token.go

Si vous avez plus de questions, vous pouvez me contacter sur https://twitch.tv/adaralex

Bonne journée à vous,

Adaralex