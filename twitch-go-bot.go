package main

import (
	"fmt"
	"github.com/gempir/go-twitch-irc"
	"strings"
	"time"
)

type CommandeTwitch struct {
	Commande   string
	Parametres []string
}

func gestionCommandeTest(commande CommandeTwitch, message twitch.PrivateMessage) {
	fmt.Println("Commande test détectée, lancée par " + message.User.DisplayName)
	// Code de la commande Test

}

func gestionCommandeBackseat(client *twitch.Client, commande CommandeTwitch, message twitch.PrivateMessage) {
	fmt.Println("Commande backseat détectée, lancée par " + message.User.DisplayName)
	// Code de la commande Backseat

	if len(commande.Parametres) > 0 {
		for _, backseateur := range commande.Parametres {
			client.Say(CHANNEL, "Hey oh, ici on ne backseat pas "+backseateur+" !!!!")
		}
	} else {
		client.Say(CHANNEL, "Hey oh, ici on ne backseat pas !")
	}

}

func gestionCommandeBack(commande CommandeTwitch, message twitch.PrivateMessage) {
	fmt.Println("Back to back coding!")
}

func getCommand(message twitch.PrivateMessage) CommandeTwitch {
	if !strings.HasPrefix(message.Message, "!") {
		return CommandeTwitch{}
	}

	fmt.Println("Recherche de commande sur " + message.Message)

	parameters := strings.Split(message.Message, " ")

	return CommandeTwitch{
		Commande:   parameters[0],
		Parametres: parameters[1:],
	}
}

func repeatMessage(client *twitch.Client) {
	ticker := time.NewTicker(30 * time.Second)
	done := make(chan bool)
	go func() {
		for {
			select {
			case <-done:
				return
			case <-ticker.C:
				// Ce bout de code permet d'envoyer un message parmi les messages connus
				//random := rand.Intn(len(REPETITIVE_COMMANDS) - 1)
				// client.Say(CHANNEL, REPETITIVE_COMMANDS[random])

				client.Say(CHANNEL, REPETITIVE_COMMANDS[repetitiveCounter])
				repetitiveCounter++
				if repetitiveCounter == len(REPETITIVE_COMMANDS) {
					repetitiveCounter = 0
				}

			}
		}
	}()
}

const CHANNEL string = "adaralex"

var repetitiveCounter = 0
var REPETITIVE_COMMANDS = []string{"Bonjour ! Il est l'heure d'écrire un message répétitif !",
	"Ceci est un autre message!",
	"Ceci est un autre test pour Atrix!"}

func main() {
	// or client := twitch.NewAnonymousClient() for an anonymous user (no write capabilities)
	// Allez sur le site https://twitchapps.com/tmi/ pour créer un token pour votre bot
	client := twitch.NewClient("Wizardy_bot", GetAuthToken())
	//client := twitch.NewAnonymousClient()

	client.OnConnect(func() {
		client.Say(CHANNEL, "Bonjour, je suis un assistant magicien, présent pour vous aider ! Wingardium Leviosa !!!")
		repeatMessage(client)
	})

	client.OnUserJoinMessage(func(message twitch.UserJoinMessage) {
		fmt.Println(message.User + " a rejoint le channel")
	})

	client.OnUserPartMessage(func(message twitch.UserPartMessage) {
		fmt.Println(message.User + " est parti du channel")
	})

	client.OnPrivateMessage(func(message twitch.PrivateMessage) {

		if message.User.DisplayName == "Nightbot" {
			return
		}

		command := getCommand(message)

		fmt.Println(message.User.DisplayName + ": " + message.Message)

		if command.Commande == "!test" {
			gestionCommandeTest(command, message)
		}

		if command.Commande == "!backseat" {
			gestionCommandeBackseat(client, command, message)
		}

		if command.Commande == "!back" {
			gestionCommandeBack(command, message)
		}

	})

	client.Join(CHANNEL)

	err := client.Connect()
	if err != nil {
		panic(err)
	}
}
